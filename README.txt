Update Exclude Unsupported
--------------------------

This module adds an option to the core update module that will prevent e-mail notifications from being sent for unsupported releases.
